package ru.NTSystem.NTConnector;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class KickRunnable implements Runnable {

	private String name;

	public KickRunnable(String name) {
		this.name = name;
	}

	@Override
	public void run() {
		@SuppressWarnings("deprecation")
		Player player = Bukkit.getPlayerExact(name);
		if (player != null)
			player.kickPlayer(NTConnectorPlugin.inst.getConfig().getString("kickmsg", "Cheating detected"));
	}

}