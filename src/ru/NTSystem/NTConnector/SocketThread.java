package ru.NTSystem.NTConnector;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class SocketThread extends Thread {
	private NTConnectorPlugin pl;

	public SocketThread(NTConnectorPlugin pl) {
		this.pl = pl;
	}

	@Override
	public void run() {
		try {
			readSocket();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void readSocket() throws IOException {
		while (!pl.stopping) {
			ServerSocket echoServer = pl.ss;
			if (echoServer.isClosed())
				return;
			Socket clientSocket = null;
			try {
				clientSocket = echoServer.accept();
				if (clientSocket.isClosed())
					return;

				pl.logInfo(
						"Connected new client: "
								+ ((InetSocketAddress) clientSocket
										.getRemoteSocketAddress())
										.getHostString(), 1);
				BufferedReader in = new BufferedReader(new InputStreamReader(
						clientSocket.getInputStream()));
				BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
						clientSocket.getOutputStream()));

				String message;
				char[] buf = new char[400];

				in.read(buf);
				message = new String(buf).trim();

				pl.logInfo("Client send message: " + message, 2);
				String answer = pl.ih.processMessage(message);
				if(answer != null){
					pl.logInfo("Our answer: " + answer, 2);
					out.write(answer);
					out.flush();
				}
				in.close();
				out.close();
				clientSocket.close();
			} catch (SocketException se) {
				// keep silent
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
