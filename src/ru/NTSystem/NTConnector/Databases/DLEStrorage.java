package ru.NTSystem.NTConnector.Databases;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import ru.NTSystem.NTConnector.NTConnectorPlugin;

public class DLEStrorage extends SQLRemoteStorage {
	
	public DLEStrorage(String host, int port, String user, String pass,
			String db) throws ClassNotFoundException, SQLException{
			super(host, port, user, pass, db);
			createTables();
	}

	@Override
	public void registerUser(String name, String password, String email) {
		int timestamp = (int) (System.currentTimeMillis() / 1000);
		sql(String.format("INSERT INTO `dle_users` (`name`, `password`, `email`, `lastdate`, `reg_date`, `info`, `signature`, `favorites`, `xfields`) VALUES "
				+ "('%s', '%s', '%s', %d, %d, '', '', '', '')",
				name, password, email, timestamp, timestamp));
	}

	@Override
	public boolean authorizeUser(String name, String password) {
		String query = String.format("SELECT COUNT(*) FROM `dle_users` WHERE `name`='%s' AND `password`='%s'", name, password);
		try {
			Connection conn = DriverManager.getConnection("jdbc:mysql://"
					+ host + ":" + port + "/" + db + "?autoReconnect=true&user=" + user
					+ "&password=" + pass);
			ResultSet rs = conn.createStatement().executeQuery(query);
			while (rs.next()) {
				if (rs.getInt(1) != 0) {
					rs.close();
					conn.close();
					return true;
				}
			}
			rs.close();
			conn.close();
			return false;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean isRegistered(String name, String email) {
		String query = String.format("SELECT COUNT(*) FROM `dle_users` WHERE `name`='%s' AND `email`='%s'", name, email);
		try {
			Connection conn = DriverManager.getConnection("jdbc:mysql://"
					+ host + ":" + port + "/" + db + "?autoReconnect=true&user=" + user
					+ "&password=" + pass);
			ResultSet rs = conn.createStatement().executeQuery(query);
			while (rs.next()) {
				if (rs.getInt(1) != 0) {
					rs.close();
					conn.close();
					return true;
				}
			}
			rs.close();
			conn.close();
			return false;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public String getType() {
		return "dle";
	}
	
	public void createTables() {
		try {
			sql(MySQLStorage.sqlUserHWIDsTable);
			sql(MySQLStorage.sqlBannedHWIDsTable);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
