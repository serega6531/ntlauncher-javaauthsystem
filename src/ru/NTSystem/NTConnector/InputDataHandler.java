package ru.NTSystem.NTConnector;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;

import ru.NTSystem.NTConnector.Databases.Storage;

public class InputDataHandler {
	private ConfigurationSection settings;
	public HashMap<String, Integer> accessList = new HashMap<String, Integer>();
	public NTConnectorPlugin pl;

	public InputDataHandler(NTConnectorPlugin pl) {
		this.pl = pl;
		this.settings = pl.settings;
		Bukkit.getScheduler().runTaskTimer(pl, new AccessListRunnable(), 20L,
				20L);
	}

	public static String getXMLData(String string, String key) {
		if (!string.contains("<" + key + ">")
				|| !string.contains("</" + key + ">"))
			return null;
		try {
			return string.substring(
					string.indexOf("<" + key + ">") + key.length() + 2,
					string.indexOf("</" + key + ">"));
		} catch (ArrayIndexOutOfBoundsException e) {
			return null;
		}
	}

	public String processMessage(String message) {
		String type = getXMLData(message, "type");
		String login = getXMLData(message, "login");
		String password = getXMLData(message, "password");
		String mail = getXMLData(message, "mail");
		String umd5 = getXMLData(message, "md5");
		String hwid = getXMLData(message, "hwid");

		boolean iswd = message.indexOf("<wd>") != -1;              // IsWatchDog
		boolean isgs = settings.getBoolean("isgs", false);         // IsGlobalSalt
		boolean swd = settings.getBoolean("swd", false);           // SaltWatchDog
		boolean wwhwid = settings.getBoolean("wwhwid", false);     // WorkWithHWID
		boolean chwidr = settings.getBoolean("chwidr", false);     // CheckHWIDOnReg
		boolean chwida = settings.getBoolean("chwida", false);     // CheckHWIDOnAuth
		String gs = settings.getString("gs");

		if ((isgs || (iswd && swd)) && !(iswd && !swd)
				&& !message.contains(gs))
			return "<response>salt fault</response>";
		
		if(type == null) return null;

		if (type.equals("reg")) {
			return processRegister(login, password, mail, hwid, chwidr,
					wwhwid);
			
		} else if (type.equals("auth")) {
			return processAuth(login, password);
			
		} else if (type.equals("gameauth")) {
			return processGameauth(umd5, chwida, login, hwid, password);
			
		} else if (type.equals("deauth")) {
			accessList.remove(login);
			Bukkit.getScheduler().runTask(pl, new KickRunnable(login));
			return "<response>success</response>";
			
		} else if (type.equals("beacon")) {
			return processBeacon(umd5, login);
		}

		return null;
	}

	private boolean isValid(String str) {
		// false is invalid
		return null != str && !(str.contains("'") || str.contains("/") || str.contains("\\")
				|| str.contains("?") || str.contains("\"") || str.contains("|") || str.contains("*")
				|| str.contains("\"") || str.contains("<") || str.contains(">") || str.contains(" "));
	}

	public String processRegister(String login, String pass, String mail,
								   String hwid, boolean chwidr, boolean wwhwid) {
		if (!isValid(login) || !isValid(pass) || !isValid(mail)
				|| !isValid(hwid)) {
			return null;
		}
		if (!Storage.getCurrentManager().isRegistered(login, mail)) {
			if (chwidr && Storage.getCurrentManager().isHWIDBanned(hwid))
				return "<response>banned</response>";
			if (wwhwid)
				Storage.getCurrentManager().addUserHWID(login, hwid);
			Storage.getCurrentManager().registerUser(login, pass, mail);
			return "<response>success</response>";
		}
		return "<response>already exists</response>";
	}

	public String processAuth(String login, String password) {
		if (!isValid(login) || !isValid(password)) {
			return null;
		}
		if (Storage.getCurrentManager().authorizeUser(login, password)) {
			return "<response>success</response><version>"
					+ settings.getInt("version") + "</version>";
		}
		return "<response>bad login</response><version>"
				+ settings.getInt("version") + "</version>";
	}

	public String processGameauth(String umd5, boolean chwida, String login,
								  String hwid, String password) {
		if (!isValid(login) || !isValid(password) || !isValid(umd5)
				|| !isValid(hwid)) {
			return null;
		}
		if (settings.getBoolean("checkmd5", false))
			if (settings.getBoolean("md5salt", false)) {
				if (!checkSaltHash(umd5, settings.getString("md5")))
					return "<response>bad checksum</response>";
			} else if (!umd5.equals(settings.getString("md5")))
				return "<response>bad checksum</response>";

		if (chwida && Storage.getCurrentManager().isHWIDBanned(hwid))
			return "<response>banned</response>";
		if (!Storage.getCurrentManager().hasUserHWID(login, hwid))
			Storage.getCurrentManager().addUserHWID(login, hwid);

		if (Storage.getCurrentManager().authorizeUser(login, password)){
			accessList.put(login, settings.getInt("timetoenter", pl.getConfig().getInt("timetoenter")));
			return "<response>success</response>";
		} else
			return "<response>bad login</response>";

	}

	public String processBeacon(String umd5, String login) {
		if (!isValid(umd5) || !isValid(login)) {
			return null;
		}
		if (settings.getBoolean("md5salt", false)) {
			if (!checkSaltHash(umd5, settings.getString("md5"))) {
				pl.logInfo("Player " + login + " removed from access list (md5-beacon)", 2);
				accessList.remove(login);
				Bukkit.getScheduler().runTask(pl, new KickRunnable(login));
				return "<response>bad checksum</response>";
			}
		} else if (!umd5.equals(settings.getString("md5"))) {
			pl.logInfo("Player " + login + " removed from access list (md5-beacon)", 2);
			accessList.remove(login);
			Bukkit.getScheduler().runTask(pl, new KickRunnable(login));
			return "<response>bad checksum</response>";
		}
		return "<response>success</response>";
	}

	private String getMD5(String input) {

		String md5 = null;

		try {
			MessageDigest digest = MessageDigest.getInstance("MD5");
			digest.update(input.getBytes(), 0, input.length());
			md5 = new BigInteger(1, digest.digest()).toString(16);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return md5;
	}

	private boolean checkSaltHash(String md5, String truemd5) {
		String salt = md5.substring(0, 2);
		md5 = md5.substring(2);
		truemd5 = getMD5(truemd5 + salt);
		return md5.equalsIgnoreCase(truemd5);
	}

	class AccessListRunnable implements Runnable {

		@Override
		public void run() {
			for (Entry<String, Integer> entry : accessList.entrySet()) {
				pl.logInfo("Cycling access list: " + entry.getKey() + " - " + entry.getValue(), 3);
				if (entry.getValue() == 0)
					accessList.remove(entry.getKey());
				entry.setValue(entry.getValue() - 1);
			}
		}

	}
}
